
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-todos',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './todos.component.html',
  styleUrl: './todos.component.css'
})
export class TodosComponent {
  todos: string[] = [];
  addTodo(newTodoInput: HTMLInputElement){
    if (newTodoInput) {
      const newTodo = newTodoInput.value.trim();
      if (newTodo !== ''){
        this.todos.unshift(newTodo);
        newTodoInput.value = '';
      } else {
        console.error('Input is empty.');
      }
    } else {
      console.error('Input element is missing.')
    }
  }

  deleteTodo(index: number) {
    this.todos.splice(index, 1);
  }
}
